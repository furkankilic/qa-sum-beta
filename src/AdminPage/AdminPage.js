import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Navbar, NavDropdown, NavItem, Nav } from 'react-bootstrap'

import { userActions } from '../_actions';

class AdminPage extends React.Component {
    componentDidMount() {
        this.props.dispatch(userActions.getAll());
    }

    handleDeleteUser(id) {
        return (e) => this.props.dispatch(userActions.delete(id));
    }

    render() {
        const { user, users } = this.props;
        return (
            <div>
                  
            <Navbar>
            <Navbar.Header>
                <Navbar.Brand>
                <a href="/"><img src="../src/img/aisolt.png" height="23px"/></a>
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
                <Nav>
                <NavItem eventKey={1} href="/question">
                    Soru-Cevap / Özet
                </NavItem>
                <NavItem eventKey={2} href="/profile">
                    Profilim
                </NavItem>
                </Nav>
                <Nav pullRight>
                <NavItem eventKey={1} href="/login">
                    Çıkış Yap
                </NavItem>
                </Nav>
            </Navbar.Collapse>
            </Navbar>
            <div className="col-md-12 col-md-offset-3">
                <h1>Hi {user.firstName}!</h1>
                <p>AISolt'a hoşgeldin vs vs.</p>
                <h3>Tüm Kayıtlı Kullanıcılar:</h3>
                {users.loading && <em>Loading users...</em>}
                {users.error && <span className="text-danger">ERROR: {users.error}</span>}
                {users.items &&
                    <ul>
                        {users.items.map((user, index) =>
                            <li key={user.id}>
                                {user.firstName + ' ' + user.lastName}
                                {
                                    user.deleting ? <em> - Deleting...</em>
                                    : user.deleteError ? <span className="text-danger"> - ERROR: {user.deleteError}</span>
                                    : <span> - <a onClick={this.handleDeleteUser(user.id)}>Delete</a></span>
                                }
                            </li>
                        )}
                    </ul>
                }
            </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedAdminPage = connect(mapStateToProps)(AdminPage);
export { connectedAdminPage as AdminPage };