import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Navbar, NavDropdown, NavItem, Nav } from 'react-bootstrap'

import { userActions } from '../_actions';

class HomePage extends React.Component {
    componentDidMount() {
        this.props.dispatch(userActions.getAll());
    }

    handleDeleteUser(id) {
        return (e) => this.props.dispatch(userActions.delete(id));
    }

    render() {
        const { user, users } = this.props;

        return (
            <div>
                  
            <Navbar>
            <Navbar.Header>
                <Navbar.Brand>
                <a href="/"><img src="../src/img/aisolt.png" height="23px"/></a>
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
                <Nav>
                <NavItem eventKey={1} href="/question">
                    Soru-Cevap / Özet
                </NavItem>
                <NavItem eventKey={2} href="/profile">
                    Profilim
                </NavItem>
                </Nav>
                <Nav pullRight>
                <NavItem eventKey={1} href="/login">
                    Çıkış Yap
                </NavItem>
                </Nav>
            </Navbar.Collapse>
            </Navbar>
            <div className="col-md-12 text-center">
                <h1>Merhaba!</h1>
                <p>AI-Solt'a hoşgeldin vs vs.</p>
            </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export { connectedHomePage as HomePage };