import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { Button, Navbar, NavItem, Nav, Row, Grid, Col } from 'react-bootstrap';
import { authHeader } from '../_helpers';




class ProfilePage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      qa: [],
      sum: []
    }
}

  componentDidMount() {
    let token = JSON.parse(localStorage.getItem('token'));
    const auth = token.token
      const requestOptions = {
          method: 'GET',
          headers: { ...authHeader(), 'Content-Type': 'application/json' }
      };
      axios.get(`https://qa-sum2.herokuapp.com/profile`, { headers: { Authorization:"Bearer " + auth } }).then(response => {
        // If request is good...
        console.log(response);
        this.setState({
          qa: response.data.qa,
          sum: response.data.summary
        })
      })
      .catch((error) => {
        console.log('error 3 ' + error);
      });

    //return fetch(`https://qa-sum2.herokuapp.com/qa`, requestOptions).then(handleResponse);

    // axios.get(`https://qa-sum2.herokuapp.com/text`) 
    //   .then(res => {
    //     const text = res.data.metin;
    //     this.setState({ 
    //       qa: text
    //     });
    //   })
  }

  render() {
    return(
      <div>
        <Navbar>
          <Navbar.Header>
            <Navbar.Brand>
            <a href="/"><img src="../src/img/aisolt.png" height="23px"/></a>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
              <NavItem eventKey={1} href="/question">
                Soru-Cevap / Özet
              </NavItem>
              <NavItem eventKey={2} href="/profile">
                Profilim
              </NavItem>
            </Nav>
            <Nav pullRight>
              <NavItem eventKey={1} href="/login">
                Çıkış Yap
              </NavItem>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Grid>
          <Row className="show-grid profile text-center">
            <Col sm={6} md={6}>
              <code>Soru-Cevaplarım</code>
              <br />
              <ul>
                <li>{this.state.qa}</li>
              </ul>  
            </Col>
            <Col sm={6} md={6}>
              <code>Özetlerim</code>
              <br />
              <ul>
                <li>{this.state.sum}</li>
              </ul>
            </Col>
          </Row>
        </Grid>
      </div>  
    )
  }
}

function mapStateToProps(state) {
  const { profile } = state.authentication;
  return {
      profile
  };
}

const connectedProfilePage = connect(mapStateToProps)(ProfilePage);
export { connectedProfilePage as ProfilePage }; 

