import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { Button, FormControl, FormGroup, ControlLabel, Panel, PanelGroup, Navbar, NavItem, Nav,} from 'react-bootstrap';
import Paragraphs from '../_components/Paragraphs';
import Clicked from '../_components/Clicked';
import { authHeader } from '../_helpers';

class QuestionPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedParagraph: '',
      pick: '',
      question: '',
      answer: '',
      kararId: '',
      sum: ''
    }
    this.answerChange = this.answerChange.bind(this);
    this.questionChange = this.questionChange.bind(this);
    this.summaryChange = this.summaryChange.bind(this);
}
  callText() {
    axios.get(`https://qa-sum2.herokuapp.com/text`) 
    .then(res => {
      console.log(res.data.kararId, 'karar')
      this.setState({
        kararId: res.data.kararId
      })
      const text = res.data.metin;
      this.setState({ 
        selectedParagraph: text
      });
    });  
  }

  componentDidMount() {
    this.callText();
    let token = JSON.parse(localStorage.getItem('token'));
    const auth = token.token
      const requestOptions = {
          method: 'GET',
          headers: { ...authHeader(), 'Content-Type': 'application/json' }
      };
      axios.get(`https://qa-sum2.herokuapp.com/qa`, { headers: { Authorization:"Bearer " + auth } }).then(response => {
        // If request is good...
        console.log(response);
        this.setState({
          qa: response.data.qa,
          sum: response.data.summary
        })
      })
      .catch((error) => {
        console.log('error 3 ' + error);
      });
  }

  refresh() {
    this.callText();
  }

  postQA() {
      const question = this.state.question
      const answer = this.state.answer
      const pick = this.state.pick
      const kararId = this.state.kararId
      const requestOptions = {
          method: 'POST',
          headers: { ...authHeader(), 'Content-Type': 'application/json' },
          body: JSON.stringify({ question, answer, pick, kararId })
      };
      console.log(question, answer)
      return fetch(`https://qa-sum2.herokuapp.com/qa`, requestOptions);
  }

  postSum() {
    const sum = this.state.sum
    const kararId = this.state.kararId
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({ sum, kararId })
    };

    return fetch(`https://qa-sum2.herokuapp.com/summary`, requestOptions);
  }

  divideToParas() {
    const paras = this.state.selectedParagraph.split(/\n\s*\n/g);
    //const paras = this.state.selectedParagraph.split("\n");
    let options = [];
    for(let i in paras) {
      options.push({value: paras[i], label: paras[i]});
    }
    return options;
  }
  
  questionChange(event) {
    const { name, value } = event.target;
    this.setState({
        question: value
    });
}

answerChange(event) {
  const { name, value } = event.target;
  this.setState({
      answer: value
  });
}

summaryChange(event) {
  const { name, value } = event.target;
  this.setState({
      sum: value
  });
}

  render() {
    const options = this.divideToParas();
    return(
      <div>

        <Navbar>
          <Navbar.Header>
            <Navbar.Brand>
              <a href="/"><img src="../src/img/aisolt.png" height="23px"/></a>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
              <NavItem eventKey={1} href="/question">
                Soru-Cevap / Özet
              </NavItem>
              <NavItem eventKey={2} href="/profile">
                Profilim
              </NavItem>
            </Nav>
            <Nav pullRight>
              <NavItem eventKey={1} href="/login">
                Çıkış Yap
              </NavItem>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <div className="left-half col-sm-6">  
          <div col-sm-12>
            <p className="dava col-sm-9 text-center">Karar</p>
            <Button 
              className="col-sm-3"
              bsStyle="default"
              bsSize="small"
              onClick={ () => this.refresh() }
              >
                <i class="fas fa-sync"></i>
            </Button>
          </div>  
          <div className="metin asd">
            <Paragraphs
              state = {this}
              name="form-field-name"
              value={this.state.selectedParagraph}
              options={options}
            />  
          </div>
       </div>  
        <div 
          className="right-half col-sm-6" 
        >
        <PanelGroup accordion className="panel" id="accordion-uncontrolled-example" defaultActiveKey="1">
          <Panel eventKey="1">
            <Panel.Heading>
              <Panel.Title toggle>Soru - Cevap</Panel.Title>
          </Panel.Heading>
          <Panel.Body collapsible>
            <p className="sag">Başlamak için bir paragraf seçiniz.</p>
            <Clicked 
              className="clicked"
              pick={this.state.pick}
            />
            <form>
              <FormGroup controlId="formControlsTextarea">
                <ControlLabel>Soru</ControlLabel>
                <FormControl componentClass="textarea" placeholder="Soru giriniz."  name="soru" value={this.state.question} onChange={this.questionChange} required/>
              </FormGroup>
              <FormGroup controlId="formControlsTextarea">
                <ControlLabel>Cevap</ControlLabel>
                <FormControl componentClass="textarea" placeholder="Cevap giriniz." name="cevap" value={this.state.answer} onChange={this.answerChange} required/>
              </FormGroup>
            </form>  
            <Button 
            bsStyle="default" 
            bsSize="medium"
            onClick={ () => this.postQA() }
            >
              Soru-Cevap Yolla
            </Button>
          </Panel.Body>
          </Panel>
          <Panel eventKey="2">
            <Panel.Heading>
              <Panel.Title toggle>Özet</Panel.Title>
            </Panel.Heading>
            <Panel.Body collapsible>


          <form>
          <FormGroup controlId="formControlsTextarea">
            <ControlLabel>Özet</ControlLabel>
            <FormControl componentClass="textarea" placeholder="Özetini giriniz." className="area" name="özet" value={this.state.sum} onChange={this.summaryChange}/>
          </FormGroup>
          </form> 
          <Button 
            bsStyle="default" 
            bsSize="medium"
            onClick={ () => this.postSum() }
          >
            Özet Yolla
          </Button>
          </Panel.Body>
          </Panel>
        </PanelGroup>

        </div> 
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { question } = state.authentication;
  return {
      question
  };
}

const connectedQuestionPage = connect(mapStateToProps)(QuestionPage);
export { connectedQuestionPage as QuestionPage }; 

