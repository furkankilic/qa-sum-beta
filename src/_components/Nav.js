import React from 'react';

export default () => {


    return (
        <div>
        <nav className="navbar navbar-default navbar-fixed-top">
          <div className="container">
            <div className="navbar-header">
              <button
                type="button"
                className="navbar-toggle collapsed"
                data-toggle="collapse"
                data-target="#navbar"
                aria-expanded="false"
                aria-controls="navbar"
              >
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar">&nbsp;</span>
                <span className="icon-bar">&nbsp;</span>
                <span className="icon-bar">&nbsp;</span>
              </button>
              <a className="navbar-brand" href="">Navbar</a>
            </div>
            <div id="navbar" className="navbar-collapse collapse">
              <ul className="nav navbar-nav">
                <li><a href="">Home</a></li>
                <li><a href="#logout">Logout</a></li>
                <li><a href="#login">Login</a></li>
              </ul>
              <ul className="nav navbar-nav navbar-right">
                <li>
                  <a href=""></a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    )
}