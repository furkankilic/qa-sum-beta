import React from 'react';

export default ({state,options}) => {
    const handleClick = function(a){
        state.setState({pick:a});
    }
    const listItem = options.map(articles => {
        return (
            <a href="javascript:void(0);" onClick={function(){handleClick(articles.value)}}>
                <li 
                    key={articles.value}
                    className="paragraph"
                    
                 >
                    {articles.value}
                </li> 
            </a>
        )
    })

    return (
    <section className="container">
        <div className="left-half">
            <article>
                <ul>
                    {listItem}
                </ul>
            </article>
        </div>
    </section>
    );
}