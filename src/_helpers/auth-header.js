export function authHeader() {
    // return authorization header with jwt token
    let token = JSON.parse(localStorage.getItem('token'));
    console.log(token.token, 'token')
    if (token.token) {
        
        return { 'Authorization': 'Bearer ' + token.token };
    } else {
        return {};
    }
}